import { Engine } from 'excalibur';

export interface IEngine extends Engine {
	columns: number;
	rows: number;
}
