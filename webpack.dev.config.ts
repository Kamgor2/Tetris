import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import * as path from 'path';
import { Configuration } from 'webpack';
import { cssRule } from './tasks/cssRule';
import { fontRule } from './tasks/fontRule';
import { htmlRule } from './tasks/htmlRule';
import { imagesRule } from './tasks/imagesRule';
import { indexHtmlRule } from './tasks/indexHtmlRule';
import { tsRule } from './tasks/tsRule';

const config: Configuration = {
	entry: {
		index: './src/index.ts',
	},
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		compress: true,
		port: 9000
	},
	mode: 'development',
	devtool: 'source-map',
	plugins: [
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'src/index.template.html'
		})
	],
	module: {
		rules: [
			{
				test: /\.ts$/,
				include: path.resolve(__dirname, 'src'),
				use: [
					'ts-loader'
				],
				exclude: /node_modules/
			}
		],
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.js'],
	},
	output: {
		filename: '[name].[hash].js',
		path: path.resolve(__dirname, 'dist'),
	},
};

export default config;
