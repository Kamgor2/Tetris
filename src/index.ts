import {Engine} from 'excalibur';
import { TetrisGame } from './Scenes/TetrisGame';

const game: Engine = new Engine({
	height: 728,
	width: 260
});

game.addScene('TetrisGame', new TetrisGame());

game.start();

game.goToScene('TetrisGame');
