import { Group } from 'excalibur';
import { Cube } from './Cube';

export abstract class Block extends Group {

	protected cubeSet: Cube[];

	public abstract toString(): string;

	public abstract getCubes(): Cube[];

}
