import { Scene } from 'excalibur';
import { IEngine } from '../../types/Engine';
import { Block } from './Block';
import { Cube } from './Cube';

export class IBlock extends Block {

	constructor(public name: string, public scene: Scene, private engine: IEngine) {
		super(name, scene);
		this.cubeSet = new Array<Cube>(4);
		this.cubeSet[0] = new Cube(0, 0);
		this.cubeSet[1] = new Cube(0, 1);
		this.cubeSet[2] = new Cube(0, 2);
		this.cubeSet[3] = new Cube(0, 3);
		this.cubeSet.forEach((cube) => {
			cube.once('grounded', () => {
				console.log('asdasd');
				cube.toggleGrounded();
				cube.row -= 1;
				this.emit('grounded');
			});
		});
	}

	public toString() {
		return 'IBlock';
	}

	public getCubes() {
		return this.cubeSet;
	}

}
