import { Actor, CollisionStartEvent, CollisionType, Color, Input, Timer, Vector } from 'excalibur';
import { IEngine } from '../../types/Engine';
import { Ground } from './Ground';

export class Cube extends Actor {

	private timer: Timer;
	public row: number;
	private column: number;
	private grounded: boolean;

	constructor(
		public x: number,
		public y: number
	) {
		super();
		this.row = x || 0;
		this.column = y || 0;
		this.grounded = false;
	}

	public onInitialize(engine: IEngine) {
		super.onInitialize(engine);
		this.color = Color.Green;
		this.setWidth(engine.drawWidth / engine.columns);
		this.setHeight(engine.drawHeight / engine.rows);
		this.pos = new Vector((this.getWidth() / 2) + (engine.drawWidth / engine.columns * this.column), (this.getHeight() / 2) + (engine.drawHeight / engine.rows * this.row));
		this.collisionType = CollisionType.Passive;
		this.on('precollision', (ev: CollisionStartEvent) => {
			this.timer.cancel();
			this.emit('grounded');
			this.grounded = true;
		});
		this.timer = new Timer(() => {
			this.row++;
		}, 300, true);
		engine.addTimer(this.timer);
	}

	public update(engine: IEngine, delta: number) {
		super.update(engine, delta);
		this.pos = new Vector((this.getWidth() / 2) + (engine.drawWidth / engine.columns * this.column), (this.getHeight() / 2) + (engine.drawHeight / engine.rows * this.row));
		if (engine.input.keyboard.wasPressed(Input.Keys.Right) && this.column < engine.columns - 1 && !this.grounded) {
			this.column += 1;
		}
		if (engine.input.keyboard.wasPressed(Input.Keys.Left) && this.column > 0 && !this.grounded) {
			this.column -= 1;
		}
	}

	public toggleGrounded() {
		this.timer.cancel();
		this.emit('grounded');
		this.grounded = true;
	}

}
