import { Actor, CollisionType, Color, Engine } from 'excalibur';
import { IEngine } from '../../types/Engine';

export class Ground extends Actor {

	public onInitialize(engine: IEngine) {
		this.color = Color.Black;
		this.setHeight(engine.drawHeight / engine.rows);
		this.pos.x = 0 + engine.drawWidth / 2;
		this.pos.y = engine.drawHeight + this.getHeight() / 2 - 10;
		this.setWidth(engine.drawWidth);
		this.collisionType = CollisionType.Fixed;
	}

	public update(engine: Engine, delta: number) {
		super.update(engine, delta);
	}
}
