import { Scene } from 'excalibur';
import { IEngine } from '../../types/Engine';
import { Block } from '../Actors/Block';
import { Cube } from '../Actors/Cube';
import { Ground } from '../Actors/Ground';
import { IBlock } from '../Actors/IBlock';

export class TetrisGame extends Scene {

	private currentBlock: Block;

	public onInitialize(engine: IEngine) {
		super.onInitialize(engine);
		engine.rows = 30;
		engine.columns = 10;
		engine.add(new Ground());
	}

	public update(engine: IEngine, delta: number) {
		super.update(engine, delta);
		if (this.currentBlock === null || this.currentBlock === undefined) {
			this.addNewBlock(engine);
		}
	}

	private addNewBlock(engine: IEngine) {
		this.currentBlock = null;
		this.currentBlock = new IBlock('Block', this, engine);
		this.currentBlock.getCubes().forEach((cube) => {
			engine.add(cube);
		});
		this.currentBlock.once('grounded', () => {
			this.addNewBlock(engine);
		});
	}

}
