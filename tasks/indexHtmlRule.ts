import { Rule } from 'webpack';

export const indexHtmlRule: Rule = {
	test: /index.template.html$/,
	use: [
		{
			loader: 'html-loader'
		}
	]
};
