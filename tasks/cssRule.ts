import * as path from 'path';
import { Rule } from 'webpack';

export const cssRule: Rule = {
	test: /\.css$/,
	include: path.resolve(__dirname, 'src'),
	use: [
		'style-loader',
		'css-loader'
	]
};
