import { Rule } from 'webpack';

export const htmlRule: Rule = {
	test: /\.html$/,
	use: [
		{
			loader: 'file-loader',
			options: {
				minimize: true,
				name: 'templates/[hash].[ext]'
			}
		}
	],
	exclude: [
		/index.template.html$/
	]
};
