import { Rule } from 'webpack';

export const fontRule: Rule = {
	test: /\.(ttf|eot|woff|woff2)$/,
	use: {
		loader: 'file-loader',
		options: {
			name: 'fonts/[name].[ext]'
		}
	}
};
