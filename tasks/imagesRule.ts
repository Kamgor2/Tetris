import { Rule } from 'webpack';

export const imagesRule: Rule = {
	test: /\.(png|jpg|jpeg|gif|svg)$/,
	use: {
		loader: 'file-loader',
		options: {
			name: 'imgs/[name].[hash].[ext]'
		}
	}
};
