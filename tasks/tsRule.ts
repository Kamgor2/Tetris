import * as path from 'path';
import { Rule } from 'webpack';


// export const tsRule: Rule = {
// 	test: /\.ts$/,
// 	include: path.resolve(__dirname, 'src'),
// 	use: [
// 		'ts-loader'
// 	],
// 	exclude: /node_modules/
// };


export const tsRule = () => {
	return {
		test: /\.ts$/,
		include: path.resolve(__dirname, 'src'),
		use: [
			'ts-loader'
		],
		exclude: /node_modules/
	};
};
